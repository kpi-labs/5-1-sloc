from analyzer.fs import read_source_file
from analyzer.Metric import Metric
import os


class FileContext:
    def __init__(self, file, basepath=''):
        self.fullpath = file
        self.file = os.path.relpath(file, basepath) if basepath else file
        self.sloc = Metric('sloc')
        self.empty_lines = Metric('empty_lines')
        self.comments = Metric('comments')
        self.physical_lines = Metric('physical_lines')
        self.logical_line = Metric('logical_line')
        self.is_multiline_comment_entered = False

    def enter_multiline_comment(self):
        self.is_multiline_comment_entered = True

    def exit_multiline_comment(self):
        self.is_multiline_comment_entered = False

    def __str__(self) -> str:
        return f"file: {self.file}\n" + (
            "\n".join(str(m) for m in self.metrics()))

    def lines(self):
        return read_source_file(self)

    def metrics(self):
        return (f for f
                in self.__dict__.values()
                if isinstance(f, Metric))
