import glob
import os

CWD = os.getcwd()


def get_files_in_project(path_to_project):
    project_path = os.path.join(CWD, os.path.abspath(path_to_project))
    return glob.iglob(project_path, recursive=True)


def read_source_file(file_context):
    with open(file_context.fullpath, 'r') as file:
        while True:
            line = file.readline()
            if not line:
                break
            yield line
