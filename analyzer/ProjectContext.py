from analyzer.FileContext import FileContext
from analyzer.fs import get_files_in_project
from analyzer.Metric import Metric
from analyzer.fs import CWD


class ProjectContext:
    def __init__(self, glob_pattern):
        self.glob_pattern = glob_pattern
        self.project_files = Metric('project_files')
        self.sloc = Metric('sloc')
        self.empty_lines = Metric('empty_lines')
        self.comments = Metric('comments')
        self.physical_lines = Metric('physical_lines')
        self.logical_line = Metric('logical_line')
        self.is_multiline_comment_entered = False

    def files(self):
        return (FileContext(f, CWD)
                for f
                in get_files_in_project(self.glob_pattern)
                )

    def add_file_to_stat(self, file):
        self.project_files.inc()
        for metric in file.metrics():
            self.__dict__[metric.name].inc(metric.value)

    def __str__(self) -> str:
        lvl_of_comments = self.comments.value/self.physical_lines.value
        return "\n".join([
            f"project: {self.glob_pattern}",
            f"level of commening: {lvl_of_comments}",
        ]+[str(m) for m in self.metrics()])

    def metrics(self):
        return (f for f
                in self.__dict__.values()
                if isinstance(f, Metric))
