import re
COMMENT_LITERAL = '#'
MULTILINE_COMMENT_LITERAL = '"""'
HAS_CONTENT_REGEX = r'[^(""")#]'
ASSIGN_REGEX = r".*[^=]=[^=].*"
FUNC_CALL_REGEX = r"\w\s+\(.*\)\s*[^:]"
CONDITIONAL_REGEX = r"([\s^](if|else|elif)\s+)"
KEYWORDS = [
    "assert",
    "async",
    "await",
    "break",
    "class",
    "continue",
    "def",
    "del",
    "elif",
    "else",
    "except",
    "finally",
    "for",
    "from",
    "global",
    "if",
    "import",
    "lambda",
    "nonlocal",
    "not",
    "pass",
    "raise",
    "return",
    "try",
    "while",
    "with",
    "yield"
]


class Line:
    def __init__(self, line, file):
        self.file = file
        self.part_of_multiline_comment = file.is_multiline_comment_entered
        self.line = line.strip()

    def is_empty(self):
        return len(self.line) == 0

    def is_single_line_comment(self):
        return (
            not self.part_of_multiline_comment
            and (
                COMMENT_LITERAL in self.line
                or (
                    self.contains_multiline_comment_open_border()
                    and self.contains_multiline_comment_close_border()
                )
            )
        )

    def contains_multiline_comment_open_border(self):
        return (
            self.line[:3] == MULTILINE_COMMENT_LITERAL
        )

    def contains_multiline_comment_close_border(self):
        return (
            self.line[-3:] == MULTILINE_COMMENT_LITERAL
        )

    def is_opens_multiline_comment(self):
        return (
            not self.part_of_multiline_comment
            and self.contains_multiline_comment_open_border()
        )

    def is_closes_multiline_comment(self):
        return (
            self.part_of_multiline_comment
            and self.contains_multiline_comment_close_border()
        )

    def has_text(self):
        return self.is_empty() and re.match(HAS_CONTENT_REGEX, self.line)

    def has_condition_keyword(self):
        return any(s in self.line for s in ['if', 'else', 'elif'])

    def conditional_flows_count(self):
        match = re.search(CONDITIONAL_REGEX, self.line)
        conds_count = len(match.groups()) if match else 0
        return conds_count

    def has_keyword(self):
        return any(s in self.line for s in KEYWORDS)

    def has_assign(self):
        return re.match(ASSIGN_REGEX, self.line)

    def has_func_call(self):
        return re.match(FUNC_CALL_REGEX, self.line)
