class Metric:
    def __init__(self, name):
        self.name = name
        self.value = 0

    def inc(self, value=1):
        self.value += value

    def __str__(self):
        return f"{self.name}: {self.value}"
