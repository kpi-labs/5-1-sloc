from analyzer.Line import Line
from analyzer.FileContext import FileContext


def process_line(line, context):
    context.sloc.inc()

    if line.is_empty():
        context.empty_lines.inc()
        return

    context.physical_lines.inc()

    if line.is_single_line_comment():
        context.comments.inc()
    elif line.is_opens_multiline_comment():
        context.enter_multiline_comment()
        if line.has_text():
            context.comments.inc()
    elif line.is_closes_multiline_comment():
        context.exit_multiline_comment()
    elif line.part_of_multiline_comment:
        context.comments.inc()
    else:
        if line.has_condition_keyword():
            context.logical_line.inc(line.conditional_flows_count())
        if line.has_keyword() or line.has_assign() or line.has_func_call():
            context.logical_line.inc()


def analyze_file(file_context):
    for source_line in file_context.lines():
        process_line(Line(source_line, file_context), file_context)

    return file_context
