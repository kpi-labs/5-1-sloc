from analyzer.ProjectContext import ProjectContext
import sys
from analyzer.analyzer import analyze_file


def main():
    project = ProjectContext(sys.argv[1])
    for file in project.files():
        project.add_file_to_stat(analyze_file(file))

    print(project)


if __name__ == '__main__':
    main()
